from unittest import mock, TestCase
import problema13

class TestProblema13(TestCase):
    def test_verifica_se_função_existe(self):
        self.assertEqual(hasattr(problema13, 'converter_em_lista'),
         True, 'Função converte_em_lista')

    def test_verifica_se_funcao_tem_parametro(self):
        self.assertEqual(problema13.converter_em_lista.__code__.co_argcount, 1, 'converter_em_lista nao possui parametros')

    def test_verifica_se_funcao_é_chamável(self):
        self.assertEqual(
            hasattr(problema13.converter_em_lista, '__call__'),
            True, 'converter_em_lista não é chamavel'
        )