from problema04 import Calc
import unittest

c = Calc()

class TestAdd(unittest.TestCase):
    def test_add_menos1_1_retorna_0(self):
        self.assertEqual(c.add(-1, 1), 0)
    def test_add_5_0_nao_retorna_0(self):
        self.assertNotEqual(c.add(5, 0), 0)    


class TestSub(unittest.TestCase):
    def test_sub_10_5_retorna_5(self):
        self.assertEqual(c.sub(10, 5), 5)
    def test_sub_menos1_5_retorna_5(self):
        self.assertEqual(c.sub(-1, 5), -6)

class TestMult(unittest.TestCase):
    def test_mult_10_1_retorna_10(self):
        self.assertEqual(c.mult(10, 1), 10)
    def test_mult_10_meio_nao_retorna_10(self):
        self.assertNotEqual(c.mult(10, 0.5), 10)

class TestDiv(unittest.TestCase):
    def test_div_5_5_nao_retorna_5(self):
        self.assertEqual(c.div(10, 1), 10)
    def test_div_10_1_retorna_10(self):
        self.assertNotEqual(c.div(5, 5), 5)

    