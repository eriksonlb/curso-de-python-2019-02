from problema08 import formula
import unittest




class TestCalc(unittest.TestCase):
    def test_soma(self):
        x, y, z, r = 2, 4, 0, 6
        self.assertEqual(formula(x, y, z), r)


    def test_sub(self):
        x, y, z, r = 0, 0, 5, -5
        self.assertEqual(formula(x, y, z), r)


    def test_formula(self):
        x, y, z, r = 2, 2, 1, 3
        self.assertEqual(formula(x, y, z), r)


    def test_formulaComplexa(self):
        x, y, z, r = 5, 5, 10, -5
        self.assertEqual(formula(formula(x, y, z), y, z), r)