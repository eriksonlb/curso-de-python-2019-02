from problema11 import formula
from unittest import TestCase, mock


class TestCalc(TestCase):
    def test_input_soma(self):
        with mock.patch('problema11.soma') as spy:
            formula(1, 1, 1)
        
        spy.assert_called_with(1, 1)
    def test_input_sub(self):
        with mock.patch('problema11.sub') as spy:
            formula(1, 1, 1)
        
        spy.assert_called_with(2, 1)
        

