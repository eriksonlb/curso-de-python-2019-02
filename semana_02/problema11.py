
def soma(x, y):
    return x + y
def sub(x, y):
    return x - y
def formula(x, y, z):
    return sub(soma(x, y), z)
def formulaComplexa(x, y, z):
    return formula(formula(x, y, z))

# print(formula(1,1,1))