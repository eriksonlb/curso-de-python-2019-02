def escreve(arquivo, texto):
    nome_arquivo = arquivo + '.txt'
    with open(nome_arquivo, 'w') as file:
        file.writelines(texto)

def copiar_texto_arquivo_para_outro(arquivo_lido, arquivo_escrito):
    with open(arquivo_escrito, 'w') as file:
        for l in open(arquivo_lido):                
            file.write(str.replace(l, ' ', '\n'))
        open(arquivo_lido).close


nome = 'exemplo'
t = 'Você precisa entender, a maioria destas pessoas não está preparada para despertar.'
escreve(nome, t)
copiar_texto_arquivo_para_outro('exemplo.txt', 'exemplo2.txt')


