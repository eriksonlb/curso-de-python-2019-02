from problema07 import formula
import unittest

class TestCalc(unittest.TestCase):
    def test_soma(self):
        self.assertEqual(formula(2, 4, 0), 6)


    def test_sub(self):
        self.assertEqual(formula(0, 0, 5), -5)


    def test_formula(self):
        self.assertEqual(formula(1, 1, 1), 1)
