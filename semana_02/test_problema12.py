from problema12 import copiar_texto_arquivo_para_outro
from unittest import TestCase, mock

class TestProblema12(TestCase):
    def test_chamada_funcao(self):
        with mock.patch('problema12.open') as spy:
            copiar_texto_arquivo_para_outro('teste_arquivo_origem.txt', 'teste_arquivo_destino.txt')
            spy.assert_called()

    def test_chamada_funcao_com_parametros(self):
        with mock.patch('problema12.open') as spy:
            copiar_texto_arquivo_para_outro('teste_arquivo_origem.txt', 'teste_arquivo_destino.txt')
            l = spy.call_args_list['teste_arquivo_origem.txt', 'teste_arquivo_destino.txt']
            mock.call(l)