# Faça um Programa que peça 2 números inteiros e um número float.
# Calcule e mostre:
#     O produto do dobro do primeiro com metade do segundo .
#     A soma do triplo do primeiro com o terceiro.
#     O terceiro elevado ao cubo.


int1 = int(input('Digite um número inteiro: '))
int2 = int(input('Digite outro número inteiro: '))
float1 = float(input('Digite um número float: '))


resultado1 = (int1 * 2) * (int2 / 2)
resultado2 = (int1 * 3) + float1
resultado3 = float1 ** 3

print('O produto do dobro de {} com metade de {} = {}'.format(int1, int2, resultado1))
print('A soma do triplo de {} com {} = {}'.format(int1, float1, resultado2))
print('{} ^ 3 = {}'.format(float1, resultado3))