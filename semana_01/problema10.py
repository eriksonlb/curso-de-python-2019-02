# Faça um programa que leia 5 números e informe o maior número.
i = 0
maior = 0
while i < 5:
    num = float(input('Digite o {}º número: '.format(i+1)))
    temp = num
    if temp > maior:
        maior = temp
    i += 1

print(maior)