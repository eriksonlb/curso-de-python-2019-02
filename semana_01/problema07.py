# Faça um programa que receba uma string e responda se ela tem alguma vogal, se sim, quais são? E também diga se ela é uma frase ou não.

text = 'Focus on science'

if text.count(' ') > 0:
    print('O texto: "{}" é uma frase e tem: '.format(text))
else:
    print('O texto {} não é uma frase e tem: '.format(text))
if 'a' in text:
    print('{} - Vogais A'.format(text.count('a')))
if 'e' in text:
    print('{} - Vogais E'.format(text.count('e')))
if 'i' in text:
    print('{} - Vogais I'.format(text.count('i')))
if 'o' in text:
    print('{} - Vogais O'.format(text.count('o')))
if 'u' in text:
    print('{} - Vogais U'.format(text.count('u'))) 

