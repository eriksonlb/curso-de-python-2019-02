# Faça uma função que receba um número, caso esse número seja múltiplo de 5,
# retorne “goiabada”, caso contrário, retorne o valor de entrada.
# EX:
# f(5) -> ‘goiabada’
# f(3) -> 3
# f(10) -> ‘goiabada’

def verificaMultiplo5(n):
    return 'goiabada' if n % 5 == 0 else n

choice = 10
print(verificaMultiplo5(choice))

