# Especifique uma função que receba um número uma string e retorne se na string
# há o mesmo número de elementos que foram passados no parâmetro
# Ex:
# f(3, ‘aaa’) -> True
# f(10, ‘Batata’) -> False

def testaString(numero: int, texto: str) -> bool:
    textoFinal = texto * numero
    if numero == len(textoFinal):
        return True
    else:
        return False

print(testaString(5, 'ee'))