# Faça um programa, com uma função, que calcula a mediana de uma lista.
# Funções embutidas que podem te ajudar:
# sorted(lista) -> ordena a lista


def mediana(lista):
    listaordenada = sorted(lista)
    posicao = len(listaordenada)    
    if posicao % 2 == 1:
        return listaordenada[posicao // 2]
    else:
        return (listaordenada[posicao // 2 - 1] + listaordenada[posicao // 2]) / 2

l = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15]
print(mediana(l))