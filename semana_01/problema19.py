# Faça um programa, com uma função, que calcule a dispersão de uma lista
# Funções embutidas que podem te ajudar:
# min(lista) -> retorna o menor valor
# max(lista) -> retorna o maior valor
def dispersao(lista):
    amplitude = max(lista) - min(lista)
    mediaLista = sum(lista) / len(lista)
    variancia = 0
    for i in lista:
        variancia += ((i - mediaLista) ** 2) / len(lista)
    
    desvioPadrao = variancia ** 0.5

    return f'Lista: {lista}\nAmplitude: {amplitude:.2f}\nVariancia: {variancia:.2f}\nDesvio Padrao: {desvioPadrao:.2f}'


l = [2, 8, 5, 6]
print(dispersao(l))


