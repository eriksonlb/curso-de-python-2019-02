# Faça um programa que peça uma nota, entre zero e dez. Mostre uma mensagem
# caso o valor seja inválido e continue pedindo até que o usuário informe um valor
# válido.
# while True:
#     n = input('Digite um número de 1 a 10: ')
#     if type(n) == int:
#         break

validador = 11
while validador < 0 or validador > 10:
    valor = int(input('Digite um numero: '))    
    print(f'Valor: {valor}')
    validador = valor
else:
    print('Fim')