# Faça um programa, com uma função que dado uma lista e uma posição da
# mesma faça o quartil dessa posição.
# p_index = int(p * len(lista))

def quartil(p, lista):
    listaOrdenada = sorted(lista)
    posicao =  (p / 4) * (len(listaOrdenada))
    if posicao % 1 == 0:
        return listaOrdenada[int(posicao)]
    else:
        return (listaOrdenada[int((posicao) // 1)] + listaOrdenada[int((posicao + 1) // 1)]) / 2

l = [1.9, 2.0, 2.1, 2.5, 3.0, 3.1, 3.3, 3.7, 6.1, 7.7]
print(quartil(1, l))
