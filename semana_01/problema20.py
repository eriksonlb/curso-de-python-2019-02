# Baseando-se nos exercícios passados, monte um dicionário com os seguintes
# seguintes chaves:
# lista, somatório, tamanho, maior valor e menor valor
lista = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20]
aurelio = {
    'lista': lista,
    'somatório': sum(lista),
    'tamanho': len(lista),
    'maior valor': max(lista),
    'menor valor': min(lista)
}

print(aurelio)