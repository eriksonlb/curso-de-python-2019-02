# Faça uma função que receba um número, caso esse número seja múltiplo de 3,
# retorne “queijo”, caso contrário, retorne o valor de entrada.
# EX:
# f(5) -> 5
# f(3) -> ‘queijo’
# f(6) -> ‘queijo’

def verificaMultiplo3(n):
    return 'queijo' if n % 3 == 0 else n

choice = 6
print(verificaMultiplo3(choice))