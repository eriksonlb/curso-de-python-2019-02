# Dada uma lista de entradas de usuário de números inteiros, construa um
# dicionário com a lista padrão, a lista dos valores elevados ao quadrado e a lista
# dos valores elevados ao cubo

lista = [2, 4, 6, 8, 10]
listaQuadrado = []
listaCubo = []

for i in lista:
    listaQuadrado.append(i ** 2)
    listaCubo.append(i ** 3)

aurelio = {
    'lista padrão': lista,
    'lista ^ 2': listaQuadrado,
    'lista ^ 3': listaCubo,    
}

print(aurelio)