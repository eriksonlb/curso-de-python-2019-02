# Faça um programa que receba uma string, com um número de ponto flutuante, e
# imprima qual a parte dele que não é inteira

text = '6.34'
textFloat = float(text)
textInt = int(textFloat)
resto = textFloat % textInt
print('A parte não inteira de {} é: {}'.format(text, round(resto, 2)))