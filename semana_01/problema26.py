# Crie uma função que faça uma saudação a alguém. A função deve receber dois
# argumentos ‘saudação’ e ‘nome’.
# Ex:
# f(‘Ahoy’, ‘Fausto’) -> ‘Ahoy Fausto’
# f(‘Olá’, bb’) -> Olá bb’

def saudacao(saudacao: str, nome: str) -> str:
    return saudacao + ' ' + nome

print(saudacao('Olá', 'Erikson'))
