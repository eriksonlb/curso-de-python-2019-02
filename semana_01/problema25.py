# Resolva de maneira imutável (Ou seja, retornando uma nova lista)
# Ex:
# Entrada = [1, 2, 3]
# Saída = [3, 2, 1]
# Coisas que podem te ajudar:
# list.insert, list.append, list.remove

l = [1, 2, 3, 4, 5]
l2 = l[::-1]
print(l, l2)