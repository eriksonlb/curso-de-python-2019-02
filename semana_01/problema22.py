# Faça um programa que itere em uma lista de maneira imperativa e que armazene em uma nova lista seu valor processado
# por uma função.
# EX:
# entrada = [‘foo’, ‘bar’, ‘spam’, ‘eggs’]
# função = f(x) = x * 2
# saida = [‘foofoo’, ‘barbar’, ‘spamspam’, ‘eggseggs’]


entrada = ['foo', 'bar', 'spam', 'eggs']
lista = []

for i in entrada:
    lista.append(i + i)

print(lista)