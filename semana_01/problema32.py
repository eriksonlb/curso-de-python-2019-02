# Faça uma função que se aplique uma função duas vezes em um valor passado
# EX:
# reaplica(soma_2, 2) -> 6
# reaplica(sub_2, 2) -> -2

def retornaDuasVezes(f: callable, n) -> int:
    return f(n)

print(retornaDuasVezes(5))