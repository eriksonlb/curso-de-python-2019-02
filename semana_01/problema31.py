# Faça uma função que receba um número, caso esse número seja múltiplo de 3 e
# 5, retorne “romeu e julieta”, caso contrário, retorne o valor de entrada.
# EX:
# f(3) -> 3
# f(5) -> 5
# f(15) -> ‘romeu e juleita’

def verificaMultiplo3e5(n):
    return 'romeu e julieta' if n != 5 and n != 3 and (n % 5 == 0 or n % 3 == 0) else n

choice = 15
print(verificaMultiplo3e5(choice))
