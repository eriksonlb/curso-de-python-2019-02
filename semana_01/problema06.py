# Faça um programa que pergunte o preço de três produtos e
# informe qual produto você deve comprar, sabendo que a decisão é sempre pelo mais barato. 

preco1 = float(input('Qual o preço do primeiro produto: '))
preco2 = float(input('Qual o preço do segundo produto: '))
preco3 = float(input('Qual o preço do terceiro produto: '))

if preco1 < preco2 or preco1 < preco3:
    menorPreco = preco1
    nomeProduto = 'primeiro produto'
if preco2 < preco1 or preco2 < preco3:
    menorPreco = preco2
    nomeProduto = 'terceiro produto'
if preco3 < preco1 or preco3 < preco2:
    menorPreco = preco3
    nomeProduto = 'segundo produto'

print('Você deve comprar o {} pois {} é o preço mais baixo'.format(nomeProduto, menorPreco))