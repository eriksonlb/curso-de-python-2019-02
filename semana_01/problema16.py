# Faça um programa, com uma função, que calcula a média de uma lista.
# Funções embutidas que podem te ajudar:
#     len(lista) -> calcula o tamanho da lista
#     sum(lista) -> faz o somatório dos valores

def mediaLista(lista):
    return sum(lista) / len(lista)


l = [5, 5, 5, 5, 5]
print(mediaLista(l))