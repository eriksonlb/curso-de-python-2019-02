# Usando os mesmos inputs do código anterior, reconstrua o problema de maneira
# declarativa.
# Funções que pode te ajudar:
# - operator.mul
# - map()

entrada = ['foo', 'bar', 'spam', 'eggs']
lista = list(map(lambda x: x *  2, entrada))
lista2 = [i + i for i in entrada]
print(lista)
print(lista2)
