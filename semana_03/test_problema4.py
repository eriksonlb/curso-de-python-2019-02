from unittest import TestCase
from unittest import mock
from problema4 import soma_1

class TestSoma_1(TestCase):
    def test_deve_retornar_2_com_1(self):
        valor_inicial = 1
        valor_esperado = 2
        self.assertEqual(soma_1(valor_inicial), valor_esperado)