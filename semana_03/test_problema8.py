from unittest import TestCase, mock
import problema8

class TestSaboresDePizza(TestCase):
    def test_sabores_de_pizza_deve_existir(self):
        self.assertEqual(hasattr(problema8, 'sabores_de_pizza'), True, 'sabores_de_pizza não existe')

    def test_sabores_de_pizza_deve_ser_uma_função(self):
        self.assertEqual(hasattr(problema8.sabores_de_pizza, '__call__'), True, 'sabores_de_pizza não é uma função')

    def test_sabores_de_pizza_deve_receber_um_parametro(self):
        self.assertEqual(problema8.sabores_de_pizza.__code__.co_argcount, 1, 'sabores_de_pizza não recebe parametro')

    def test_parametro_deve_ser_float(self):
        with mock.patch('problema8.sabores_de_pizza') as spy:
            problema8.sabores_de_pizza('teste')
        assert spy.
        

    # def test_sabores_de_pizza_deve_retornar_uma_tupla(self):
        
    
    # def test_primeiro_parametro_deve_ser_string(self):
    #     with mock.patch()