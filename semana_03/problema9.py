def ler_arquivo(arquivo):
    try:
        file = open(arquivo)
    except FileNotFoundError:
        print(f'O {arquivo} não existe')
    else: 
        file.read()
        print('Sucesso')
    finally:
        print('Fim da aplicação')
        # finally executa se o try passou ou não

ler_arquivo('teste.txt')
