from unittest import TestCase, mock
from io import StringIO
import problema7

class TestWriteData(TestCase):
    def test_verifica_se_write_data_recebe_parametro(self):
        self.assertEqual(
            problema7.write_data.__code__.co_argcount,
            1,
            'write_data não recebe dois parametros'
            )

    def test_write_data_deve_escrever_no_arquivo(self):
        buffer =  FakeBuffer()
        with patch('p_7.open', return_value=buffer) as mocked_open:
            write_data('batatinha')
