# language:pt

Funcionalidade: fizzbuzz
    Eu como usuário desejo saber se um determinado número é fizz, buzz ou fizzbuzz

    Cenario: 3 é um número fizz
        Quando o fizzbuzz for chamado com o número 3
        Então 3 é fizz
    
    Cenario: 5 é um número buzz
        Quando o fizzbuzz for chamado com o número 5
        Então 5 é buzz

    Cenario: 15 é um número fizzbuzz
        Quando o fizzbuzz for chamado com o número 15
        Então 15 é fizzbuzz
    
    Cenario: 2 é o número 2
        Quando o fizzbuzz for chamado com o número 2
        Então 2 é 2